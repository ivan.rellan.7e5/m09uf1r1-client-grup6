using System;
using System.Collections;
using System.Text;
using Model.Login;
using Model.Network;
using Newtonsoft.Json;
using UnityEngine.Networking;

namespace Network
{
    public class HttpHandler
    {
        private readonly string _baseURL;
        public string BaseURL => _baseURL;

        public HttpHandler(string baseURL)
        {
            _baseURL = baseURL;
        }
        
        public IEnumerator GetRequest<TOutput>(
            string endpoint, 
            Action<TOutput> onSuccess, 
            Action<IHttpError> onError
        )
        {
            string path = _baseURL + endpoint;
            using (UnityWebRequest webRequest = UnityWebRequest.Get(path))
            {
                webRequest.SetRequestHeader("Content-Type", "application/json");

                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                switch (webRequest.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        onError(new ConnectionError());
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        var code = webRequest.responseCode;
                        var message = webRequest.downloadHandler.text;
                        onError(new ServerError(code, message));
                        break;
                    case UnityWebRequest.Result.Success:
                        TOutput result = JsonConvert.DeserializeObject<TOutput>(webRequest.downloadHandler.text);
                        onSuccess(result);
                        break;
                }
            }
        }

        public IEnumerator PostRequest<TInput, TOutput>(
            string endpoint, 
            TInput body,
            Action<TOutput> onSuccess,
            Action<IHttpError> onError
        )
        {
            yield return PostRequest(
                endpoint, 
                body,
                delegate(TInput _, TOutput output) { onSuccess(output); },
                onError
            );
        }

        public IEnumerator PostRequest<TInput, TOutput>(
            string endpoint,
            TInput body,
            Action<TInput, TOutput> onSuccess, 
            Action<IHttpError> onError
        ) {
            string path = _baseURL + endpoint;
            using (UnityWebRequest webRequest = new UnityWebRequest(path, "POST"))
            {
                string bodystring = JsonConvert.SerializeObject(body);
                // convert JSON to raw bytes
                byte[] rawBody = Encoding.UTF8.GetBytes(bodystring);
                webRequest.uploadHandler = new UploadHandlerRaw(rawBody);
                webRequest.downloadHandler = new DownloadHandlerBuffer();
                webRequest.SetRequestHeader("Content-Type", "application/json");

                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                switch (webRequest.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        onError(new ConnectionError());
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        var code = webRequest.responseCode;
                        var message = webRequest.downloadHandler.text;
                        onError(new ServerError(code, message));
                        break;
                    case UnityWebRequest.Result.Success:
                        var text = webRequest.downloadHandler.text;
                        TOutput result = JsonConvert.DeserializeObject<TOutput>(text);
                        onSuccess(body, result);
                        break;
                }
            }
        }
        
        public IEnumerator PutRequest<TInput, TOutput>(
            string endpoint,
            TInput body,
            Action<TOutput> onSuccess,
            Action<IHttpError> onError
        )
        {
            yield return PutRequest(
                endpoint, 
                body,
                delegate(TInput _, TOutput output) { onSuccess(output); },
                onError
            );
        }

        public IEnumerator PutRequest<TInput, TOutput>(
            string endpoint,
            TInput body,
            Action<TInput, TOutput> onSuccess, 
            Action<IHttpError> onError
        ) {
            string path = _baseURL + endpoint;
            using (UnityWebRequest webRequest = new UnityWebRequest(path, "PUT"))
            {
                string bodystring = JsonConvert.SerializeObject(body);
                // convert JSON to raw bytes
                byte[] rawBody = Encoding.UTF8.GetBytes(bodystring);
                webRequest.uploadHandler = new UploadHandlerRaw(rawBody);
                webRequest.downloadHandler = new DownloadHandlerBuffer();
                webRequest.SetRequestHeader("Content-Type", "application/json");

                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                switch (webRequest.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        onError(new ConnectionError());
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        var code = webRequest.responseCode;
                        var message = webRequest.downloadHandler.text;
                        onError(new ServerError(code, message));
                        break;
                    case UnityWebRequest.Result.Success:
                        var text = webRequest.downloadHandler.text;
                        TOutput result = JsonConvert.DeserializeObject<TOutput>(text);
                        onSuccess(body, result);
                        break;
                }
            }
        }
    }
}