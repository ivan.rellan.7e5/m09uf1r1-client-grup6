using System;
using System.Collections;
using System.Collections.Generic;
using Model.Login;
using Model.Network;
using Model.Ranking;
using Newtonsoft.Json;
using SocketIOClient;
using UnityEngine;

namespace Managers
{
    public class SocketHandler : Patterns.Singleton<SocketHandler>{
        private readonly string _baseURL = "https://mafia-clicker.herokuapp.com/";
        public string BaseURL => _baseURL;
        private SocketIOUnity socket;
        public SocketIOUnity Socket => socket;
        private bool connectionConfirmed;

        private List<Action<object, EventArgs>> onConnect;
        private List<Action<object, EventArgs>> onDisconnect;
        public delegate void Connect();
        public static event Connect OnConnect;
        static void CallOnConnect()
        {
            OnConnect?.Invoke();
        }

        public delegate void Disconnect(object obj, string message);
        public static event Disconnect OnDisconnect;
        static void CallOnDisconnect(object obj, string message)
        {
            OnDisconnect?.Invoke(obj, message);
        }

        public delegate void ConnectionRejected();
        public static event ConnectionRejected OnConnectionRejected;
        static void CallOnConnectionRejected()
        {
            OnConnectionRejected?.Invoke();
        }
        
        public delegate void MessageReceived(string message, SocketIOResponse response);
        public static event MessageReceived OnMessageReceived;
        static void CallOnMessageReceived(string message, SocketIOResponse response)
        {
            OnMessageReceived?.Invoke(message, response);
        }

        private readonly List<string> listOfEvents = new()
        {
            "ping",
            "user",
            "users",
            "changeUsername",
            "getRanking",
            "updateScore",
            "deleteScore"
        };

        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            OnMessageReceived += ConnectionConfirmation;
        }

        private void OnDestroy()
        {
            if(socket is {Connected: true})
                socket.Disconnect();
            OnMessageReceived -= ConnectionConfirmation;
        }
        
        void SetHeaders(params Header[] headers)
        {
            if(socket is {Connected: true})
                socket.Disconnect();
            socket = new SocketIOUnity(_baseURL, new SocketIOOptions
            {
                Query = new Dictionary<string, string> {{"token", "UNITY"}},
                ExtraHeaders = new Dictionary<string, string>(Header.ToDictionary(headers)),
                Transport = SocketIOClient.Transport.TransportProtocol.WebSocket
            });
        }

        IEnumerator WaitToCheckConnectionRejection()
        {
            yield return new WaitForSeconds(5f);
            if (!connectionConfirmed)
            {
                CallOnConnectionRejected();
                socket.Disconnect();
            }
        }

        void ConnectionConfirmation(string message, SocketIOResponse response)
        {
            if (message == "ping")
            {
                var result = GetTypedResponse<ServerStatusDto>(response);
                Debug.Log(result.ToString());
                connectionConfirmed = true;
                CallOnConnect();
            }
        }

        void SetUpMessageReceivedEvents()
        {
            foreach (var message in listOfEvents)
            {
                socket.On(message, response =>
                {
                    UnityThread.executeInUpdate(() => CallOnMessageReceived(message, response));
                });
            }
        }
        
        public void Authenticate(string user, string password)
        {
            SetHeaders(
                new Header("querytype", "authentication"),
                new Header("mail", user),
                new Header("password", password)
            );

            socket.OnConnected += (_, _) => UnityThread.executeInUpdate(() => LoginManager.Instance.SignIn(user));
            socket.OnDisconnected += (obj, message) => UnityThread.executeInUpdate(() => CallOnDisconnect(obj, message));
            SetUpMessageReceivedEvents();
            connectionConfirmed = false;
            StartCoroutine(WaitToCheckConnectionRejection());
            socket.ConnectAsync().ContinueWith(t => {});
        }

        public void Register(
            string username,
            string mail,
            string password,
            bool superuser
        ) {
            SetHeaders(
                new Header("querytype", "newUser"),
                new Header("mail", mail),
                new Header("password", password),
                new Header("username", username),
                new Header("superuser", superuser ? "true" : "false")
            );
            
            socket.OnConnected += (_, _) => UnityThread.executeInUpdate(() => LoginManager.Instance.SignIn(mail));
            socket.OnDisconnected += (obj, message) => UnityThread.executeInUpdate(() => CallOnDisconnect(obj, message));
            SetUpMessageReceivedEvents();
            connectionConfirmed = false;
            StartCoroutine(WaitToCheckConnectionRejection());
            socket.Connect();
        }

        public void DisconnectConnection()
        {
            if(socket is {Connected: true})
                socket.Disconnect();
            else
                CallOnDisconnect(null, null);
        }

        public void ThrowMessage(string message)
        {
            if(socket is {Connected: true})
                socket.Emit(message);
            else if (socket == null)
                Debug.Log("No socket");
            else
                Debug.Log("Socket not connected");
        }
        
        public void ThrowMessage<T>(string message, T data)
        {
            if(socket is {Connected: true})
                socket.Emit(message, JsonConvert.SerializeObject(data));
            else if (socket == null)
                Debug.Log("No socket");
            else
                Debug.Log("Socket not connected");
        }

        public static T GetTypedResponse<T>(SocketIOResponse response)
        {
            var result = JsonConvert.DeserializeObject<List<T>>(response.ToString());
            return result![0];
        }
    }
}
