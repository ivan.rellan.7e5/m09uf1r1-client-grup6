﻿using System.Collections;
using Patterns;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    class GameNavigationManager : Singleton<GameNavigationManager>
    {
        [SerializeField] private GameObject gameScreen;
        public GameObject GameScreen => gameScreen;

        [SerializeField] private GameObject rankingScreen;
        public GameObject RankingScreen => rankingScreen;

        [SerializeField] private GameObject errorPanel;
        public GameObject ErrorPanel => errorPanel;

        void Start()
        {
            ActivateScreen(gameScreen);
            LoginManager.OnLogOut += GoBackToLogin;
        }

        private void OnDestroy()
        {
            LoginManager.OnLogOut -= GoBackToLogin;
        }

        void ActivateScreen(GameObject screen)
        {
            screen.SetActive(true);
            if (screen.TryGetComponent(out UiPanel panel)) panel.OnStart();
        }

        void DeactivateScreen(GameObject screen)
        {
            if (screen.TryGetComponent(out UiPanel panel)) panel.OnEnd();
            screen.SetActive(false);
        }

        public void GoToScreen(GameObject from, GameObject to)
        {
            DeactivateScreen(from);
            ActivateScreen(to);
        }

        void GoBackToLogin()
        {
            AudioManager.Instance.Play("ErrorSound");
            errorPanel.SetActive(true);
            StartCoroutine(Wait3SecondsAndGoToLogin());
        }

        IEnumerator Wait3SecondsAndGoToLogin()
        {
            yield return new WaitForSeconds(3f);
            DeactivateScreen(gameScreen);
            AudioManager.Instance.Stop("gameSong");
            SceneManager.LoadScene("LoginScreen");
        }
    }
}
