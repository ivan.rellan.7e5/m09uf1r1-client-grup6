using System;
using Model;
using Patterns;
using UnityEngine;
using UnityEngine.Audio;

namespace Managers
{
    public class AudioManager : Singleton<AudioManager>
    {
        public Sound[] sounds;
        [SerializeField] private AudioMixer soundMixer;
        public AudioMixer SoundMixer => soundMixer;
        [SerializeField] private AudioMixer musicMixer;
        public AudioMixer MusicMixer => musicMixer;

        protected override void Awake()
        {
            DontDestroyOnLoad(gameObject);
            foreach (var sound in sounds)
            {
                sound.source = gameObject.AddComponent<AudioSource>();
                sound.source.clip = sound.clip;

                sound.source.volume = sound.volume;
                sound.source.pitch = sound.pitch;

                sound.source.outputAudioMixerGroup = sound.output;
            }
        }

        public void Play(string name)
        {
            Play(name, false);
        }
        
        public void Play(string name, bool loop)
        {
            Sound sound = Array.Find(sounds, sound => sound.name == name);
            if (sound != null)
            {
                sound.source.loop = loop;
                sound.source.Play();
            }
        }

        public void Stop(string name)
        {
            Sound sound = Array.Find(sounds, sound => sound.name == name);
            if (sound != null)
                sound.source.Stop();
        }
        
        public void ResetMixers()
        {
            SetMixerValue(PlayerPrefs.GetFloat("SoundSlider", 1), AudioManager.Instance.SoundMixer);
            SetMixerValue(PlayerPrefs.GetFloat("MusicSlider", 1), AudioManager.Instance.MusicMixer);
        }
        
        public void SetMixerValue(float sliderValue, AudioMixer mixer)
        {
            float valIndB = -80 + 40 * Mathf.Pow(2, sliderValue);
            if (sliderValue < 0.01)
                valIndB = -120;
            mixer.SetFloat("Volume", valIndB);
        }
    }
}