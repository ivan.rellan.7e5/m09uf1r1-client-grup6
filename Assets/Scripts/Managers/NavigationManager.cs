using System;
using Patterns;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    public class NavigationManager : Singleton<NavigationManager>
    {
        [SerializeField] private GameObject loginScreen;
        public GameObject LoginScreen => loginScreen;
        [SerializeField] private GameObject registrationScreen;
        public GameObject RegistrationScreen => registrationScreen;
        [SerializeField] private GameObject successfulRegistrationScreen;
        public GameObject SuccessfulRegistrationScreen => successfulRegistrationScreen;
        [SerializeField] private GameObject gameMenuScreen;
        public GameObject GameMenuScreen => gameMenuScreen;
        [SerializeField] private GameObject soundMenuScreen;
        public GameObject SoundMenuScreen => soundMenuScreen;

        void Start()
        {
            ActivateScreen(loginScreen);
        }

        void ActivateScreen(GameObject screen)
        {
            screen.SetActive(true);
            if(screen.TryGetComponent(out UiPanel panel)) panel.OnStart();
        }

        void DeactivateScreen(GameObject screen)
        {
            if(screen.TryGetComponent(out UiPanel panel)) panel.OnEnd();
            screen.SetActive(false);
        }   

        public void GoToScreen(GameObject from, GameObject to)
        {
            DeactivateScreen(from);
            ActivateScreen(to);
        }

        public void GoToGameScene(GameObject from)
        {
            DeactivateScreen(from);
            SceneManager.LoadScene("GameScene");
        }
    }
}