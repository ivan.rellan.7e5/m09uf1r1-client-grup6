using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Model.Ranking;
using SocketIOClient;
using Menus;
using Model.Login;

namespace Managers
{
    public class GlobalRankingManager : Patterns.Singleton<GlobalRankingManager>
    {
        private List<ScoreUpdateDto> _ranking = new();
        public List<ScoreUpdateDto> Ranking => _ranking;

        private void Start()
        {
            SocketHandler.OnMessageReceived += RetrieveMark;
            SocketHandler.OnMessageReceived += GetRanking;
            SocketHandler.Instance.ThrowMessage("updateScore", new PositionDto(LoginManager.Instance.Mail, 0));
            InvokeRepeating("CreatePositionDtoAndSend", 2.0f, 2.0f);
        }

        private void OnDestroy()
        {
            SocketHandler.OnMessageReceived -= RetrieveMark;
            SocketHandler.OnMessageReceived -= GetRanking;
        }

        void RetrieveMark(string message, SocketIOResponse response)
        {
            if (message == "updateScore")
            {
                MarkManager.Instance.CallOnClick();
            }
        }

        void GetRanking(string message, SocketIOResponse response)
        {
            if (message == "getRanking")
            {
                _ranking = SocketHandler.GetTypedResponse<List<ScoreUpdateDto>>(response);
                RankingUI.CallOnRanking();
            }
        }

        void SendMark(
            PositionDto positionDto
        ) {
            SocketHandler.Instance.ThrowMessage("updateScore", positionDto);
        }

        public void CreatePositionDtoAndSend()
        {
            PositionDto positionDto = new PositionDto(
                LoginManager.Instance.Mail,
                MarkManager.Instance.AccumulatedMark
            );

            MarkManager.Instance.ClearAccumulatedMark();
            SendMark(positionDto);
        }
    }
}
