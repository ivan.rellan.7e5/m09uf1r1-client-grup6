using UnityEngine;
using UnityEngine.UI;

namespace Managers
{
    public class ClickerManager : MonoBehaviour
    {
        private int _clicksBeforeChange = 5; // <-- Can be changed
        [SerializeField] private int actualClicks;
        public GameObject[] catsList;
        private int indexCatsList;
        private GameObject actualCat;
        private ParticleSystem _particleSystem;
        [SerializeField] private GameObject particles;

    
        void Start()
        {
            actualClicks = _clicksBeforeChange + 1;
            _particleSystem = particles.GetComponent<ParticleSystem>();

            AudioManager.Instance.Play("gameSong");
        }

        void Update()
        {
            if (actualClicks >= _clicksBeforeChange) 
                ChangeCat();
        }
    
        void OnClick()
        {
            actualClicks++;
            particles.SetActive(true);
            _particleSystem.Emit(10);
        }

        void ChangeCat()
        {
            int index = Random.Range(1, 6);
            AudioManager.Instance.Play("cat" + index);
            GlobalRankingManager.Instance.CreatePositionDtoAndSend();
            actualClicks = 0;
            DestroyImmediate(actualCat, true);

            indexCatsList = Random.Range(0, catsList.Length);
            actualCat = Instantiate(catsList[indexCatsList], GameObject.Find("GamePanel").transform, true);
            actualCat.transform.transform.position = GameObject.Find("GamePanel").transform.position;
            actualCat.transform.localScale = new Vector3(3,3,3);

            var button = GameObject.Find("Button_Cat").GetComponent<Button>();
            button.onClick.AddListener(OnClick);
            button.onClick.AddListener(MarkManager.Instance.AddMark);
        }
    }
}
