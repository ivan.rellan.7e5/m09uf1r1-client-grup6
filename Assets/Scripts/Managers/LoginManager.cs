using JetBrains.Annotations;
using UnityEngine;
using Model.Login;
using System.Collections.Generic;
using SocketIOClient;

namespace Managers
{
    public class LoginManager : Patterns.Singleton<LoginManager>
    {
        [SerializeField] private bool isLoggedIn;
        public bool IsLoggedIn => isLoggedIn;
        [SerializeField] [CanBeNull] private string mail;
        public string Mail => mail;

        public delegate void LogIn();
        public static event LogIn OnLogIn;
        public static void CallOnLogIn() { if (OnLogIn != null) OnLogIn(); }
        
        public delegate void LogOut();
        public static event LogOut OnLogOut;
        public static void CallOnLogOut() { if (OnLogOut != null) OnLogOut(); }

        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }

        protected virtual void Start()
        {
            isLoggedIn = false;
            mail = null;
            
            SocketHandler.OnDisconnect += (_, _) => UnityThread.executeInUpdate(SignOut);
        }

        internal void SignIn(string mailDto)
        {
            isLoggedIn = true;
            mail = mailDto;
            
            CallOnLogIn();
        }

        void SignOut()
        {
            isLoggedIn = false;
            mail = null;
            
            CallOnLogOut();
        }

        public void Authenticate(
            string user,
            string password
        ) {
            SocketHandler.Instance.Authenticate(user, password);
        }

        public void RegisterUser(
            string username, 
            string mail, 
            string password, 
            bool superuser
        ) {
            SocketHandler.Instance.Register(
                username,
                mail,
                password,
                superuser
            );
        }
    }
}
