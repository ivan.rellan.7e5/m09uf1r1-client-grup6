using UnityEngine;

namespace Player
{
    public class SpeedController : MonoBehaviour
    {
        [SerializeField] private float speedMultiplier;
        [SerializeField] private float multiplierDivider;
        [SerializeField] private Animator animator;
        private static readonly int SpeedMultiplier = Animator.StringToHash("SpeedMultiplier");

        void Start()
        {
            if (animator == null)
                animator = GetComponent<Animator>();

            if (multiplierDivider == 0)
                multiplierDivider = 100;

            MarkManager.OnClick += SetSpeedMultiplier;
        }

        private void OnDestroy()
        {
            MarkManager.OnClick -= SetSpeedMultiplier;
        }

        void SetSpeedMultiplier()
        {
            if (MarkManager.Instance.TotalMark > 0)
                animator.SetFloat(SpeedMultiplier, MarkManager.Instance.TotalMark / multiplierDivider);
        }
    }
}
