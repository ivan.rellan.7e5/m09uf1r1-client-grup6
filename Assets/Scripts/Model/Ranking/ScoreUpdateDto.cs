using System;
using Newtonsoft.Json;

namespace Model.Ranking
{
    [Serializable]
    public class ScoreUpdateDto
    {
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }
        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }
        [JsonProperty(PropertyName = "score")]
        public int Score { get; set; }

        public ScoreUpdateDto(string userName, string mail, int score)
        {
            UserName = userName;
            Mail = mail;
            Score = score;
        }
    }
}