using System;
using Newtonsoft.Json;

namespace Model.Ranking
{
    [Serializable]
    public class PositionDto
    {
        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }
        [JsonProperty(PropertyName = "score")]
        public int Score { get; set; }

        public PositionDto(string mail, int score)
        {
            Mail = mail;
            Score = score;
        }
    }
}