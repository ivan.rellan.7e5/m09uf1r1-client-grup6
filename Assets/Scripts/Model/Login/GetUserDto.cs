using System;
using Newtonsoft.Json;

namespace Model.Login
{
    [Serializable]
    public class GetUserDto
    {
        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }

        public GetUserDto(string mail)
        {
            Mail = mail;
        }
    }
}