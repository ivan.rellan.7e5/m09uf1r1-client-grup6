using System;
using Newtonsoft.Json;

namespace Model.Login
{
    [Serializable]
    public class UserCredentialsDto
    {
        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }
        [JsonProperty(PropertyName = "superuser")]
        public bool Superuser { get; set; }

        public UserCredentialsDto(string mail, string password, string username, bool superuser)
        {
            Mail = mail;
            Password = password;
            Username = username;
            Superuser = superuser;
        }

        public override string ToString()
        {
            return "Username: " + Username + "; Mail: " + Mail + "; Password: " + Password +
                   (Superuser ? "; Superuser;" : ";");
        }
    }
}