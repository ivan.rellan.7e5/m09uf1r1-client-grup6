using System;
using Newtonsoft.Json;

namespace Model.Login
{
    [Serializable]
    public class NewUsernameDto
    {
        [JsonProperty(PropertyName = "newusername")]
        public string NewUsername { get; set; }
    }
}