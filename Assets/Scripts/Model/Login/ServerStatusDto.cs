using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SocketIOClient;

namespace Model.Login
{
    [Serializable]
    public class ServerStatusDto
    {
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
        [JsonProperty(PropertyName = "version")]
        public string Version { get; set; }

        public override string ToString()
        {
            return "Server: " + Message + " Version: " + Version;
        }

        [CanBeNull]
        public static ServerStatusDto GetServerStatusDto(SocketIOResponse response)
        {
            var result = JsonConvert.DeserializeObject<List<ServerStatusDto>>(response.ToString());
            if (result != null && result.Any())
                return result[0];
            return null;
        }
    }
}