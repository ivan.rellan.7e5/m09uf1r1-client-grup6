using System;
using Newtonsoft.Json;

namespace Model.Login
{
    [Serializable]
    public class UpdateUsernameDto
    {
        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }
        [JsonProperty(PropertyName = "newusername")]
        public string NewUsername { get; set; }

        public UpdateUsernameDto(string mail, string newUsername)
        {
            Mail = mail;
            NewUsername = newUsername;
        }
    }
}