using System;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace Model.Login
{
    [Serializable]
    public class IsUserDto
    {
        [JsonProperty(PropertyName = "isUser")]
        public bool IsUser { get; set; }
        [JsonProperty(PropertyName = "username")]
        [CanBeNull] public string Username { get; set; }
    }
}