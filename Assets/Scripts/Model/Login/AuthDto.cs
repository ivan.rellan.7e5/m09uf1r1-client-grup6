using System;
using Newtonsoft.Json;

namespace Model.Login
{
    [Serializable]
    public class AuthDto
    {
        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        public AuthDto(string mail, string password)
        {
            Mail = mail;
            Password = password;
        }
    }
}