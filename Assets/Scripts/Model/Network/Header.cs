using System.Collections.Generic;
using System.Linq;

namespace Model.Network
{
    public class Header
    {
        public string Key;
        public string Value;

        public Header(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public static Dictionary<string, string> ToDictionary(Header[] list)
        {
            var dictionary = new Dictionary<string, string>();
            foreach (var header in list)
                dictionary.Add(header.Key, header.Value);
            return dictionary;
        }
    }
}