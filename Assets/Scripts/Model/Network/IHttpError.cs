namespace Model.Network
{
    public enum HttpError
    {
        ConnectionError,
        InvalidQuery,
        MailIsTaken,
        MailDoesNotExist,
        InvalidCredentials,
        SomethingWentWrong,
        PlayerNotFound
    }
    
    public interface IHttpError
    {
        public HttpError HttpError { get; }
    }

    public class ConnectionError : IHttpError
    {
        public HttpError HttpError => HttpError.ConnectionError;
        
        
        public override string ToString()
        {
            return "Connection Error.";
        }
    }

    public class ServerError : IHttpError
    {
        private readonly long _code;
        public long Code => _code;
        private readonly string _message;
        public string Message => _message;
        private HttpError _httpError;
        public HttpError HttpError => _httpError;

        private static ServerError[] ListOfErrors => 
            new[]
            {
                new ServerError(422, "Invalid Query Parameters", HttpError.InvalidQuery),
                new ServerError(422, "Mail Is Taken", HttpError.MailIsTaken),
                new ServerError(422, "Mail Does Not Exist", HttpError.MailDoesNotExist),
                new ServerError(422, "Invalid Credentials", HttpError.InvalidCredentials),
                new ServerError(500, "Internal Error", HttpError.SomethingWentWrong),
                new ServerError(422, "Player not found", HttpError.PlayerNotFound)
            };
        

        public ServerError(long code, string message, HttpError httpError)
        {
            _code = code;
            _message = message;
            _httpError = httpError;
        }

        public ServerError(long code, string message)
        {
            _code = code;
            _message = message;

            var list = ListOfErrors;
            bool found = false;
            for (int i = 0; !found && i < list.Length; i++)
            {
                found = _code == list[i]._code && _message == list[i]._message;
                if(found) 
                    _httpError = list[i]._httpError;
            }
            if (!found)
                _httpError = HttpError.SomethingWentWrong;
        }

        public override string ToString()
        {
            return "Error " + _code + ": " + _message + ".";
        }

        public override int GetHashCode()
        {
            return 64 * (int) _code + _message.GetHashCode();
        }

        public sealed override bool Equals(object obj)
        {
            if (!(obj is ServerError))
                return false;
            var objAsError = (ServerError) obj;
            if (_code != objAsError._code)
                return false;
            if (_message != objAsError._message)
                return false;
            return true;
        }

        public static IHttpError InvalidCredentials =>
            new ServerError(422, "Invalid Credentials", HttpError.InvalidCredentials);
    }
}
