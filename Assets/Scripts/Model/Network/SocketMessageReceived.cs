using System;
using SocketIOClient;

namespace Model.Network
{
    public class SocketMessageReceived
    {
        public string Message;
        public Action<SocketIOResponse> OnMessageReceived;

        public SocketMessageReceived(string message, Action<SocketIOResponse> onMessageReceived)
        {
            Message = message;
            OnMessageReceived = onMessageReceived;
        }
    }
}