using UnityEngine;

public class MarkManager : Patterns.Singleton<MarkManager>
{
    [SerializeField] private int _accumulatedMark;
    [SerializeField] private int _totalMark;
    [SerializeField] private int _multiplier;

    public delegate void Click();
    public static event Click OnClick;
    public void CallOnClick() { if (OnClick != null) OnClick(); }

    public int AccumulatedMark => _accumulatedMark;
    public int TotalMark => _totalMark;
    public int Multiplier => _multiplier;

    // Start is called before the first frame update
    void Start()
    {
        _totalMark = PlayerPrefs.GetInt("score", 0);
        _multiplier = PlayerPrefs.GetInt("multi", 1);
    }

    public void AddMark()
    {
        _accumulatedMark += 1 * Multiplier;
        _totalMark += 1 * Multiplier;
        PlayerPrefs.SetInt("score", _totalMark);
        CallOnClick();
    }

    public void SetTotalMark(int newTotalMark)
    {
        _totalMark = newTotalMark;
        PlayerPrefs.SetInt("score", _totalMark);
    }

    public void ClearAccumulatedMark()
    {
        _accumulatedMark = 0;
    }

    public void ApplyMultiplier(int newMultiplier)
    {
        _multiplier *= newMultiplier;
        PlayerPrefs.SetInt("multi", _multiplier);
    }

    public void ClearMultiplier()
    {
        _multiplier = 1;
        PlayerPrefs.SetInt("multi", 1);
    }

    public void ClearMultiplier(int oldMultiplier)
    {
        _multiplier /= oldMultiplier;
        PlayerPrefs.SetInt("multi", _multiplier);
    }
}
