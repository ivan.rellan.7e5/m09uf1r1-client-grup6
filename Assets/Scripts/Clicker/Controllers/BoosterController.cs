using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;
using UnityEngine.UI;

public class BoosterController : MonoBehaviour
{
    [SerializeField] private int _multiplier;
    [SerializeField] private int _multiplierDuration;
    [SerializeField] private int _multiplierCooldown;
    private BoosterUsedEnum _boosterState;

    private MarkManager _markController;
    private Button _ownButton;

    // Start is called before the first frame update
    void Start()
    {
        if (_multiplier == 0)
            _multiplier = 2;

        if (_multiplierDuration == 0)
            _multiplierDuration = 5;

        if (_multiplierCooldown == 0)
            _multiplierCooldown = 10;

        _markController = MarkManager.Instance;

        _ownButton = GetComponent<Button>();
        
    }

    public void ApplyMultiplier()
    {
        if (_boosterState == BoosterUsedEnum.FREE)
            StartCoroutine(Booster());
    }

    IEnumerator Booster()
    {
        AudioManager.Instance.Play("ButtonClick");
        _boosterState = BoosterUsedEnum.USED;
        _markController.ApplyMultiplier(_multiplier);
        _ownButton.enabled = false;
        yield return new WaitForSeconds(_multiplierDuration);
        _markController.ClearMultiplier(_multiplier);
        yield return new WaitForSeconds(_multiplierCooldown);
        _boosterState = BoosterUsedEnum.FREE;
        _ownButton.enabled = true;
    }
}
