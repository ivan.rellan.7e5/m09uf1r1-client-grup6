using Patterns;
using UnityEngine;
using UnityEngine.UI;
using Managers;
using Model.Ranking;

namespace Menus
{
    public class RankingUI : UiPanel
    {
        [SerializeField] private GameObject _row;
        [SerializeField] private Transform _rowsParent;

        public delegate void Ranking();
        public static event Ranking OnRanking;
        public static void CallOnRanking() { if (OnRanking != null) OnRanking(); }

        public override void OnStart()
        {
            // _rows = GameObject.FindGameObjectsWithTag("Row");
            SocketHandler.Instance.ThrowMessage("getRanking");
            OnRanking += SetLeaderboard;
        }

        public override void OnEnd()
        {
            RemoveLeaderboard();
        }

        public void SetLeaderboard()
        {
            RemoveLeaderboard();
            foreach(ScoreUpdateDto position in GlobalRankingManager.Instance.Ranking)
            {
                GameObject newGo = Instantiate(_row, _rowsParent);
                Text[] texts = newGo.GetComponentsInChildren<Text>();
                texts[0].text = position.UserName;
                texts[1].text = position.Score.ToString();
            }
        }

        public void RemoveLeaderboard()
        {
            GameObject[] children = GameObject.FindGameObjectsWithTag("Row");
            foreach (GameObject child in children)
            {
                Destroy(child);
            }
        }

        public void OnGoBackClick()
        {
            AudioManager.Instance.Play("ButtonClick");
            GameNavigationManager.Instance.GoToScreen(gameObject, GameNavigationManager.Instance.GameScreen);
        }
    }
}
