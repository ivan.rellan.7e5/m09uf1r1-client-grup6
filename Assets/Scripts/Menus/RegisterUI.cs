using Managers;
using Patterns;
using UnityEngine;

namespace Menus
{
    public class RegisterUI : UiPanel
    {
        [SerializeField] private Color textDefaultColor;
        [SerializeField] private Color textErrorColor;
        [TextArea(4,20)][SerializeField] private string mailIsTakenText;
        [TextArea(4,20)][SerializeField] private string invalidCredentialsText;
        [TextArea(4,20)][SerializeField] private string connectionErrorText;
        [TextArea(4,20)][SerializeField] private string somethingWentWrongText;
        
        [SerializeField] private GameObject errorText;
        [SerializeField] private GameObject usernameInput;
        [SerializeField] private GameObject mailInput;
        [SerializeField] private GameObject passwordInput;
        [SerializeField] private GameObject registerButton;

        public override void OnStart()
        {
            SetDefaultState();
            LoginManager.OnLogIn += GoToGameMenu;
            SocketHandler.OnConnectionRejected += OnConnectionRejection;
        }

        public override void OnEnd()
        {
            LoginManager.OnLogIn -= GoToGameMenu;
            SocketHandler.OnConnectionRejected -= OnConnectionRejection;
        }

        public void OnRegisterClick()
        {
            AudioManager.Instance.Play("ButtonClick");
            string username = GetInputFieldText(usernameInput);
            string mail = GetInputFieldText(mailInput);
            string password = GetInputFieldText(passwordInput);
            SetWaitingState();
            LoginManager.Instance.RegisterUser(
                username, 
                mail, 
                password, 
                false
            );
        }

        public void GoBackButton()
        {
            AudioManager.Instance.Play("ButtonClick");
            NavigationManager.Instance.GoToScreen(
                gameObject,
                NavigationManager.Instance.LoginScreen
            );
        }

        void OnConnectionRejection()
        {
            AudioManager.Instance.Play("ErrorSound");
            SetErrorState();
        }

        void SetDefaultState()
        {
            SetInputFieldText(usernameInput, "");
            SetInputFieldText(mailInput, "");
            SetInputFieldText(passwordInput, "");
            SetText(errorText, "");
            
            EnableButton(registerButton);
            EnableInputField(mailInput);
            EnableInputField(passwordInput);
            EnableInputField(usernameInput);
            
            SetInputFieldColor(mailInput, textDefaultColor);
            SetInputFieldColor(passwordInput, textDefaultColor);
        }

        void SetWaitingState()
        {
            SetText(errorText, "");
            
            DisableButton(registerButton);
            DisableInputField(mailInput);
            DisableInputField(passwordInput);
            DisableInputField(usernameInput);
            
            SetInputFieldColor(mailInput, textDefaultColor);
            SetInputFieldColor(passwordInput, textDefaultColor);
        }

        void SetErrorState()
        {
            EnableButton(registerButton);
            EnableInputField(mailInput);
            EnableInputField(passwordInput);
            EnableInputField(usernameInput);
            SetText(errorText, somethingWentWrongText);
        }

        void GoToGameMenu()
        {
            AudioManager.Instance.Play("SuccessSound");
            NavigationManager.Instance.GoToScreen(
                gameObject,
                NavigationManager.Instance.SuccessfulRegistrationScreen
            );
        }
    }
}
