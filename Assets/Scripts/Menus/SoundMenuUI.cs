using Managers;
using Patterns;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Menus
{
    public class SoundMenuUI : UiPanel
    {
        [SerializeField] public GameObject soundSlider;
        [SerializeField] public GameObject musicSlider;

        public override void OnStart()
        {
            LoginManager.OnLogOut += SetLoginUI;
            ResetSliders();
        }

        public override void OnEnd()
        {
            LoginManager.OnLogOut += SetLoginUI;
        }

        public void OnGoBackClick()
        {
            AudioManager.Instance.Play("ButtonClick");
            SetGameMenuUI();
        }
        
        public void OnSoundVolumeChange()
        {
            OnSliderChange(soundSlider, AudioManager.Instance.SoundMixer);
        }

        public void OnMusicVolumeChange()
        {
            OnSliderChange(musicSlider, AudioManager.Instance.MusicMixer);
        }

        void SetLoginUI()
        {
            AudioManager.Instance.Play("ErrorSound");
            NavigationManager.Instance.GoToScreen(
                gameObject, 
                NavigationManager.Instance.LoginScreen
            );
        }

        void SetGameMenuUI()
        {
            NavigationManager.Instance.GoToScreen(
                gameObject,
                NavigationManager.Instance.GameMenuScreen
            );
        }
        
        public void ResetSliders()
        {
            soundSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("SoundSlider", 1);
            musicSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("MusicSlider", 1);
        }
        
        void OnSliderChange(GameObject slider, AudioMixer mixer)
        {
            float val = slider.GetComponent<Slider>().value;
            AudioManager.Instance.SetMixerValue(val, mixer);
            PlayerPrefs.SetFloat(slider.name, slider.GetComponent<Slider>().value);
        }
    }
}
