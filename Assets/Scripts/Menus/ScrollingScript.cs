using UnityEngine;
using UnityEngine.UI;
using Vector2 = UnityEngine.Vector2;

namespace Menus
{
    public class ScrollingScript : MonoBehaviour
    {
        [SerializeField] private float speedOfScroll = 1f;
        [SerializeField] private Vector2 directionOfScroll = new Vector2(1,0);
        private Vector2 _scrollSpeed;
        void Start()
        {
            _scrollSpeed = directionOfScroll.normalized * speedOfScroll;
        }

        void Update()
        {
            var rect = GetComponent<RawImage>().uvRect;
            GetComponent<RawImage>().uvRect = new Rect(rect.position += _scrollSpeed * Time.deltaTime, rect.size);
        }
    }
}
