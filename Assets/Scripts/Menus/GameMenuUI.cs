using System;
using System.Collections.Generic;
using Managers;
using Model.Login;
using Model.Ranking;
using Patterns;
using SocketIOClient;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menus
{
    public class GameMenuUI : UiPanel
    {
        public override void OnStart()
        {
            LoginManager.OnLogOut += SetLoginUI;
            SocketHandler.OnMessageReceived += SetPlayerPrefScore;
            SocketHandler.Instance.ThrowMessage("updateScore", new PositionDto(LoginManager.Instance.Mail, 0));
            /*
            SocketHandler.OnMessageReceived += DebugListOfUsers;
            SocketHandler.OnMessageReceived += DebugCurrentUser;
            SocketHandler.Instance.ThrowMessage("users");
            SocketHandler.Instance.ThrowMessage("user", new GetUserDto(LoginManager.Instance.Mail)); */
        }

        public override void OnEnd()
        {
            LoginManager.OnLogOut -= SetLoginUI;
            SocketHandler.OnMessageReceived -= SetPlayerPrefScore;
            /*
            SocketHandler.OnMessageReceived -= DebugListOfUsers;
            SocketHandler.OnMessageReceived -= DebugCurrentUser;*/
        }

        void SetPlayerPrefScore(string message, SocketIOResponse response)
        {
            if (message == "updateScore")
            {
                var result = SocketHandler.GetTypedResponse<ScoreUpdateDto>(response);
                PlayerPrefs.SetInt("score", result.Score);
                PlayerPrefs.SetInt("multi", 1);
            }
        }

        public void OnGoBackButton()
        {
            AudioManager.Instance.Play("ButtonClick");
            SocketHandler.Instance.DisconnectConnection();
        }

        public void OnGoToSoundMenuClick()
        {
            AudioManager.Instance.Play("ButtonClick");
            SetAudioMenu();
        }

        void SetLoginUI()
        {
            NavigationManager.Instance.GoToScreen(
                gameObject, 
                NavigationManager.Instance.LoginScreen
            );
        }

        void SetAudioMenu()
        {
            NavigationManager.Instance.GoToScreen(
                gameObject,
                NavigationManager.Instance.SoundMenuScreen
            );
        }
        
        void LoadGameScene()
        {
            AudioManager.Instance.Play("ButtonClick");
            AudioManager.Instance.Stop("Elevatorstuck");
            NavigationManager.Instance.GoToGameScene(gameObject);
        }
    }
}
