using System;
using Managers;
using Model.Login;
using Model.Network;
using Patterns;
using UnityEngine;

namespace Menus
{
    public class LoginUI : UiPanel
    {
        [SerializeField] private Color textDefaultColor;
        [SerializeField] private Color textErrorColor;
        [TextArea(4,20)][SerializeField] private string invalidCredentialsText;
        [TextArea(4,20)][SerializeField] private string connectionErrorText;
        [TextArea(4,20)][SerializeField] private string somethingWentWrongText;
        
        [SerializeField] private GameObject errorText;
        [SerializeField] private GameObject mailInput;
        [SerializeField] private GameObject passwordInput;
        [SerializeField] private GameObject loginButton;
        [SerializeField] private GameObject registerButton;

        void Start()
        {
            AudioManager.Instance.ResetMixers();
            AudioManager.Instance.Play("Elevatorstuck", true);
        }

        public override void OnStart()
        {
            SetDefaultState();
            LoginManager.OnLogIn += SetGameMenuScreen;
            SocketHandler.OnConnectionRejected += OnConnectionRejection;
        }

        public override void OnEnd()
        {
            LoginManager.OnLogIn -= SetGameMenuScreen;
            SocketHandler.OnConnectionRejected -= OnConnectionRejection;
        }

        public void OnLogInClick()
        {
            AudioManager.Instance.Play("ButtonClick");
            string mail = GetInputFieldText(mailInput);
            string password = GetInputFieldText(passwordInput);
            SetWaitingAnswerState();
            LoginManager.Instance.Authenticate(
                mail, 
                password
            );
        }

        public void OnRegisterClick()
        {
            AudioManager.Instance.Play("ButtonClick");
            NavigationManager.Instance.GoToScreen(
                gameObject, 
                NavigationManager.Instance.RegistrationScreen
            );
        }

        void OnConnectionRejection()
        {
            AudioManager.Instance.Play("ErrorSound");
            SetErrorState();
        }

        void SetDefaultState()
        {
            SetText(errorText, "");
            SetInputFieldText(mailInput, "");
            SetInputFieldText(passwordInput, "");
            
            EnableButton(loginButton);
            EnableButton(registerButton);
            EnableInputField(mailInput);
            EnableInputField(passwordInput);
            SetInputFieldColor(mailInput, textDefaultColor);
            SetInputFieldColor(passwordInput, textDefaultColor);
        }

        void SetWaitingAnswerState()
        {
            SetText(errorText, "");
            DisableButton(loginButton);
            DisableButton(registerButton);
            DisableInputField(mailInput);
            DisableInputField(passwordInput);
            SetInputFieldColor(mailInput, textDefaultColor);
            SetInputFieldColor(passwordInput, textDefaultColor);
        }

        void SetErrorState()
        {
            EnableButton(loginButton);
            EnableButton(registerButton);
            EnableInputField(mailInput);
            EnableInputField(passwordInput);
            SetText(errorText, somethingWentWrongText);
        }

        void SetGameMenuScreen()
        {
            AudioManager.Instance.Play("SuccessSound");
            NavigationManager.Instance.GoToScreen(
                gameObject,
                NavigationManager.Instance.GameMenuScreen
            );
        }
    }
}
