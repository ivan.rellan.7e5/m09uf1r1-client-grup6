using System.Collections;
using Managers;
using Patterns;
using UnityEngine;
using UnityEngine.Serialization;

namespace Menus
{
    public class RedirectToLogin : UiPanel
    {
        [SerializeField] private float waitingTime = 3f;

        public override void OnStart()
        {
            StartCoroutine(SetLoginScreenAfterSeconds(waitingTime));
        }
        
        IEnumerator SetLoginScreenAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            NavigationManager.Instance.GoToScreen(
                gameObject,
                NavigationManager.Instance.GameMenuScreen
            );
        }
    }
}
