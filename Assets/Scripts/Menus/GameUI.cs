using Patterns;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Managers;

namespace Menus
{
    public class GameUI : UiPanel
    {
        [SerializeField] private Text _title;

        public override void OnStart()
        {
            if (_title == null)
                _title = GameObject.Find("Score").GetComponent<Text>();

            SetScoreTitle();
            MarkManager.OnClick += SetScoreTitle;
        }

        public override void OnEnd()
        {
            MarkManager.OnClick -= SetScoreTitle;
        }

        public void SetScoreTitle()
        {
            _title.text = "SCORE: " + MarkManager.Instance.TotalMark;
        }

        public void OnRankingClick()
        {
            AudioManager.Instance.Play("ButtonClick");
            GlobalRankingManager.Instance.CreatePositionDtoAndSend();
            GameNavigationManager.Instance.GoToScreen(gameObject, GameNavigationManager.Instance.RankingScreen);
        }
    }
}
