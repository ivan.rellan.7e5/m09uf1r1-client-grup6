using System;
using UnityEngine;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

namespace Patterns
{
    // Expects Buttons to have 1 child: Text
    // Expects InputField to have 2 children: Text, Placeholder
    public abstract class UiPanel : MonoBehaviour
    {
        public abstract void OnStart();

        public virtual void OnEnd() { }

        protected void DisableButton(GameObject button)
        {
            button.GetComponent<Button>().interactable = false;
        }

        protected void EnableButton(GameObject button)
        {
            button.GetComponent<Button>().interactable = true;
        }

        protected void DisableInputField(GameObject inputText)
        {
            inputText.GetComponent<InputField>().interactable = false;
        }

        protected void EnableInputField(GameObject inputText)
        {
            inputText.GetComponent<InputField>().interactable = true;
        }

        protected void SetInputFieldColor(GameObject inputText, Color color)
        {
            inputText.GetComponent<RectTransform>().Find("Text").GetComponent<Text>().color = color;
        }

        protected string GetInputFieldText(GameObject inputField)
        {
            return inputField.GetComponent<InputField>().text;
        }
        
        protected void SetInputFieldText(GameObject inputField, string text)
        {
            inputField.GetComponent<InputField>().text = text;
        }

        protected void SetText(GameObject textGo, string newText)
        {
            textGo.GetComponent<Text>().text = newText;
        }
    }
}