using Managers;

public class MockSocketHandler : LoginManager
{
    protected override void Start()
    {
        base.Start();
        Authenticate("edu@itb.cat", "314");
    }
}
